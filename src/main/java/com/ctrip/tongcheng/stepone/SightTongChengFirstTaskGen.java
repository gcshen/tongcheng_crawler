package com.ctrip.tongcheng.stepone;

/**
 * Created by gcshen on 13-12-30.
 */

import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import com.ctrip.tongcheng.steptwo.SightTongChengSecondCoreWorker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SightTongChengFirstTaskGen implements IGenerator{
    private static Logger log = LoggerFactory.getLogger(SightTongChengSecondCoreWorker.class);
    private static List<SightTongChengFirstTaskDetail> taskList = getTasksFromDB();
    private static int taskIndex = 0;

    @Override
    public Task genTask() {
        if (taskIndex == taskList.size()) {
            log.info("第一步任务结束！");
            return null;
        }
        SightTongChengFirstTaskDetail taskDetail = taskList.get(taskIndex);
        taskIndex++;

        Task task = new Task();
        task.setId(1);
        task.setBuilder("SightTongChengFirstTest Builder");
        task.setBuildTime(System.currentTimeMillis());
        task.setLastModify(System.currentTimeMillis());
        task.setTaskDetail(taskDetail);
        return task;
    }

    @Override
    public String genTaskStr() {
        return null;
    }
    private static List<SightTongChengFirstTaskDetail> getTasksFromDB() {
        List<SightTongChengFirstTaskDetail> tasks = new ArrayList<SightTongChengFirstTaskDetail>();
        //只需一条记录
        String url = "http://www.17u.cn/scenery/SearchList.aspx?&action=getlist&page=pageindex&kw=&pid=0&cid=0&cyid=0&theme=0&grade=0&money=0&sort=0&paytype=0&ismem=0&istuan=0&isnow=0&iid=0.08102992083877325";
        SightTongChengFirstTaskDetail taskDetail = new SightTongChengFirstTaskDetail();
        taskDetail.setUrl(url);
        tasks.add(taskDetail);
        return tasks;
    }
}
