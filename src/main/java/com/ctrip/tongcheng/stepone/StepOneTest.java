package com.ctrip.tongcheng.stepone;

/**
 * Created by gcshen on 13-12-30.
 */

import com.ctrip.Utils.MySqlHelper;
import com.ctrip.common.Utils;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.dao.DBHelper;
import com.ctrip.fx.enteroctopus.common.dao.DBHelperBuilder;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class StepOneTest {
    public static void Test() {
        System.out.println("开始第一步：");
        Utils.loadConfig();

        //创建任务生成器和抓取器
        IGenerator tonghengFirstGenerator = new SightTongChengFirstTaskGen();
        ICoreWorker tongchengFirstCorWorker = new SightTongChengFirstCoreWorker();
        System.out.println(new Date());
        Task firstTask;
        //只执行一次，取出总页数，分配好爬取页任务后入库
        if ((firstTask = tonghengFirstGenerator.genTask()) != null) {
            Result result = tongchengFirstCorWorker.execute(firstTask);
            if (result != null) {
                //写入数据库
                MySqlHelper.SaveEntities(result.getResultDetail().getDataList());
            }
        }
    }
}
