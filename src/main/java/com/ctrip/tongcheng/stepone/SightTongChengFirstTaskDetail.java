package com.ctrip.tongcheng.stepone;

/**
 * Created by gcshen on 13-12-30.
 */

import com.ctrip.fx.enteroctopus.common.jpa.DBColumn;
import com.ctrip.fx.enteroctopus.common.jpa.DBEntity;
import com.ctrip.fx.enteroctopus.common.module.TaskDetail;

import java.sql.Types;

@DBEntity(tableName = "fx_octopus_task_detail_tongcheng_sight_first", useSP = true, dailyTable = false)
public class SightTongChengFirstTaskDetail extends TaskDetail {
    @DBColumn(columnName = "url", length = 200, nullable = true, spOutputType = Types.VARCHAR)
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
