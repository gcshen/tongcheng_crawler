package com.ctrip.tongcheng.stepone;

/**
 * Created by gcshen on 13-12-30.
 */

import com.ctrip.common.ResultFactory;
import com.ctrip.network.Command;
import com.ctrip.network.Executor;
import com.ctrip.network.Response;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.fx.enteroctopus.common.util.NetworkUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by gcshen on 13-12-30.
 */

public class SightTongChengFirstCoreWorker implements ICoreWorker {

    private static Logger log = LoggerFactory.getLogger(SightTongChengFirstCoreWorker.class);

    @Override
    public Result execute(Task task) {
        SightTongChengFirstResultDetail firstResultDetail = new SightTongChengFirstResultDetail();
        try {
            SightTongChengFirstTaskDetail taskDetail = (SightTongChengFirstTaskDetail) task.getTaskDetail();
            long taskBuildTime = task.getBuildTime();
            String url = taskDetail.getUrl();
            //抓取html
            Document document = getResponse(url);
            //解析html
            SightInfoTongChengFirstDecodeDetail firstDecodeDetail = null;
            if (document == null) {
                System.out.println("获取Document为空！"+url);
            } else {
                SightTongChengFirstParser SightTongChengFirstParser = new SightTongChengFirstParser();
                firstDecodeDetail = SightTongChengFirstParser.getResultDetail(document, url);
            }
            if (firstDecodeDetail == null) {
                log.info("Document解析为SightInfoTongChengFirstDecodeDetail出错！");
            } else {
                try {
                    //分配页数任务
                    int pageCount = firstDecodeDetail.getPageCount();
                    Date spiderTime =new Date();
                    int pageSize = 10;
                    for (int i = 0; i < pageCount / pageSize; i++) {
                        SightInfoTongChengFirstDecodeDetail tInfo = new SightInfoTongChengFirstDecodeDetail();
                        tInfo.setPageCount(pageCount);
                        tInfo.setPageSize(pageSize);
                        tInfo.setBeginPageIndex(i * pageSize + 1);
                        tInfo.setEndPageIndex((i + 1) * pageSize);
                        tInfo.setSpiderTime(spiderTime);
                        tInfo.setTaskBuildTime(taskBuildTime);
                        firstResultDetail.getDataList().add(tInfo);
                    }
                    if (pageCount % pageSize > 0) {
                        SightInfoTongChengFirstDecodeDetail tInfo = new SightInfoTongChengFirstDecodeDetail();
                        tInfo.setPageCount(pageCount);
                        tInfo.setPageSize(pageCount % pageSize);
                        int count = pageCount / pageSize;
                        tInfo.setBeginPageIndex(count * pageSize + 1);
                        tInfo.setEndPageIndex(pageCount);
                        tInfo.setSpiderTime(spiderTime);
                        tInfo.setTaskBuildTime(taskBuildTime);
                        firstResultDetail.getDataList().add(tInfo);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    log.info("=====Exception Info:" + e.getMessage());
                    log.info("分配景点页任务任务出错");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("============Exception Info:" + e.getMessage());
            log.info("第一步执行同程景点数抓取失败！");
        }
        ResultFactory rf=ResultFactory.instance();
        Result firstTaskResult =rf.getResult(task);
        firstTaskResult.setTask(task);
        firstTaskResult.setResultDetail(firstResultDetail);
        return  firstTaskResult;
    }

    private Document getResponse(String url) {
        Document doc = null;
        Executor executor = new Executor();
        Command command = new Command();
        command.setUrl(url);
        command.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,**/;q=0.8");
        command.addHeader("Accept-Encoding", "gzip");
        command.addHeader("Accept-Language","zh-CN,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        command.addHeader("Connection", "keep-alive");
        command.addHeader("DNT", "1");
        command.addHeader("Host", "www.17u.cn");
        command.addHeader("Referer", "http://www.17u.cn/scenery/scenerysearchlist_0_0__0_0_0.html");
        command.addHeader("User-Agent", NetworkUtils.getNewerUserAgentrandomly());
        Response response = new Response();
        executor.executeGetGzip(command, response);
        String content = null;
        if (response.getResultCode() == Response.Success && response.getContent() != null) {
            content = new String(response.getContent());
        }
        if (content != null) {
            doc = Jsoup.parse(content);
        }
        return doc;
    }
}