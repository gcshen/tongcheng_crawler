package com.ctrip.tongcheng.stepone;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by gcshen on 13-12-30.
 */
public class SightTongChengFirstParser {
    private static Logger log = LoggerFactory.getLogger(SightTongChengFirstParser.class);
    public SightInfoTongChengFirstDecodeDetail getResultDetail(
            Document htmlDocument,String url) {

        SightInfoTongChengFirstDecodeDetail product = new SightInfoTongChengFirstDecodeDetail();
        try {

            product.setSpiderTime(new Date());
            product.setPageCount(getPageCount(htmlDocument));

        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("=====Exception Info:" + ex.getMessage());
            log.info("解析页面{}时出错！",url);
        }
        return product;
    }



    private int getPageCount(Document document) {
        int pageNumber=0;
        try {
            Element element = document.getElementById("txt_AllpageNumber");
            String value = element.val();
            pageNumber = Integer.parseInt(value);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            log.info("=====Exception Info:" + e.getMessage());
            log.info("获取页面数出错");
        }
        return pageNumber;
    }
}
