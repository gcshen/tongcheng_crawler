package com.ctrip.tongcheng.stepone;


import com.ctrip.fx.enteroctopus.common.jpa.DBColumn;
import com.ctrip.fx.enteroctopus.common.jpa.DBEntity;
import com.ctrip.fx.enteroctopus.common.jpa.DBId;

import java.sql.Types;
import java.util.Date;

/**
 * Created by gcshen on 13-12-30.
 */
@DBEntity(tableName = "fx_octopus_result_detail_sight_tongcheng_first", useSP = true, dailyTable = false)
public class SightInfoTongChengFirstDecodeDetail {
    @DBId
    @DBColumn(spParamIndex = 2, spOutput = true, spOutputType = Types.BIGINT)
    private  long  id;

    @DBColumn(columnName="pageCount",nullable = true,spOutputType = Types.INTEGER)
    private int pageCount;

    @DBColumn(columnName="pageSize",nullable = true,spOutputType = Types.INTEGER)
    private int pageSize;

    @DBColumn(columnName="beginPageIndex",nullable = true,spOutputType = Types.INTEGER)
    private int beginPageIndex;

    @DBColumn(columnName="endPageIndex",nullable = true,spOutputType = Types.INTEGER)
    private int endPageIndex;

    @DBColumn(columnName="spiderTime",nullable = true, spOutputType = Types.DATE)
    private Date spiderTime;

    @DBColumn(columnName="taskBuildTime",nullable = true, spOutputType = Types.BIGINT)
    private long taskBuildTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getBeginPageIndex() {
        return beginPageIndex;
    }

    public void setBeginPageIndex(int beginPageIndex) {
        this.beginPageIndex = beginPageIndex;
    }

    public int getEndPageIndex() {
        return endPageIndex;
    }

    public void setEndPageIndex(int endPageIndex) {
        this.endPageIndex = endPageIndex;
    }

    public Date getSpiderTime() {
        return spiderTime;
    }

    public void setSpiderTime(Date spiderTime) {
        this.spiderTime = spiderTime;
    }

    public long getTaskBuildTime() {
        return taskBuildTime;
    }

    public void setTaskBuildTime(long taskBuildTime) {
        this.taskBuildTime = taskBuildTime;
    }
}
