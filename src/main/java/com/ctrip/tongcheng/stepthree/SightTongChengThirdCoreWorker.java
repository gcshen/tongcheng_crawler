package com.ctrip.tongcheng.stepthree;

import com.ctrip.common.ResultFactory;
import com.ctrip.network.Command;
import com.ctrip.network.Executor;
import com.ctrip.network.Response;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.fx.enteroctopus.common.util.NetworkUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gcshen on 13-12-31.
 */
public class SightTongChengThirdCoreWorker implements ICoreWorker {
    private static Logger log = LoggerFactory.getLogger(SightTongChengThirdCoreWorker.class);
    @Override
    public Result execute(Task task) {
        SightTongChengThirdResultDetail thirdResultDetail = new SightTongChengThirdResultDetail();
        try {
            SightTongChengThirdTaskDetail taskDetail = (SightTongChengThirdTaskDetail) task.getTaskDetail();
            Document document;
            SightTongChengThirdParser sightTongChengThirdParser = new SightTongChengThirdParser();
            SightInfoTongChengThirdDecodeDetail thirdDecodeDetail;
            document = getResponse(taskDetail.getSightUrl());
            if (document == null) {
                System.out.println("没有爬取页面："+taskDetail.getSightUrl());
                SightTongChengThirdTaskGen.task_surplus.add(taskDetail);
            } else {
                thirdDecodeDetail = sightTongChengThirdParser.getResultDetail(document,taskDetail.getSightUrl());
                thirdDecodeDetail.setSightname(taskDetail.getSightName());
                thirdDecodeDetail.setTaskBuildTime(task.getBuildTime());
                thirdResultDetail.getDataList().add(thirdDecodeDetail);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("============Exception Info:" + e.getMessage());
            log.info("第三步执行第"+task.getId()+"个同程景点内容抓取失败！");
        }
        ResultFactory rf=ResultFactory.instance();
        Result thirdTaskResult =rf.getResult(task);
        thirdTaskResult.setResultDetail(thirdResultDetail);
        thirdTaskResult.setTask(task);
        return thirdTaskResult;
    }

    private Document getResponse(String url) {
        Document doc = null;
        Executor executor = new Executor();
        Command command = new Command();
        command.setUrl(url);
        command.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,**/;q=0.8");
        command.addHeader("Accept-Encoding", "gzip");
        command.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        command.addHeader("Connection", "keep-alive");
        command.addHeader("DNT", "1");
        command.addHeader("Host", "www.17u.cn");
        command.addHeader("Referer", "http://www.17u.cn/scenery/scenerysearchlist_0_0__0_0_0.html");
        command.addHeader("User-Agent", NetworkUtils.getNewerUserAgentrandomly());
        Response response = new Response();
        try {
            executor.executeGetGzip(command, response);
            String content = null;
            if (response.getResultCode() == Response.Success && response.getContent() != null) {
                content = new String(response.getContent());
            }
            if (content != null) {
                doc = Jsoup.parse(content);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info(e.getMessage());
            log.info("=======抓取当前页面出错:" + url);
            return null;
        }
        return doc;
    }
}
