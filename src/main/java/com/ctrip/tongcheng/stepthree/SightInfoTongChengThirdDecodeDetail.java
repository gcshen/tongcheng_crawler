package com.ctrip.tongcheng.stepthree;

import com.ctrip.fx.enteroctopus.common.jpa.DBColumn;
import com.ctrip.fx.enteroctopus.common.jpa.DBEntity;
import com.ctrip.fx.enteroctopus.common.jpa.DBId;

import java.sql.Types;
import java.util.Date;

/**
 * Created by gcshen on 13-12-31.
 */
@DBEntity(tableName = "fx_octopus_result_detail_sight_tongcheng_third")
public class SightInfoTongChengThirdDecodeDetail {
    @DBId
    @DBColumn(columnName = "id", spParamIndex = 2, spOutput = true, spOutputType = Types.BIGINT)
    private long id;
    @DBColumn(columnName = "sightid", nullable = true, spOutputType = Types.BIGINT)
    private long sightid;
    @DBColumn(columnName = "sightname", nullable = true, spOutputType = Types.VARCHAR, length = 100)
    private String sightname;
    @DBColumn(columnName = "sightlevel", nullable = true, spOutputType = Types.CHAR, length = 6)
    private String sightlevel;
    @DBColumn(columnName = "sighturl", nullable = true, spOutputType = Types.VARCHAR, length = 100)
    private String sighturl;
    @DBColumn(columnName = "sightticketprice", nullable = true, spOutputType = Types.VARCHAR,length =200 )
    private String sightticketprice;
    @DBColumn(columnName = "spiderTime", nullable = true, spOutputType = Types.DATE)
    private Date spiderTime;
    @DBColumn(columnName = "taskBuildTime", nullable = true, spOutputType = Types.BIGINT)
    private long taskBuildTime;
    @DBColumn(columnName = "sightfatherurl", nullable = true, spOutputType = Types.VARCHAR, length = 100)
    private String sightfatherurl;
    @DBColumn(columnName = "sightaddress", nullable = true, spOutputType = Types.VARCHAR, length = 200)
    private String sightaddress;
    @DBColumn(columnName = "sighttransport", nullable = true, spOutputType = Types.LONGNVARCHAR)
    private String sighttransport;
    @DBColumn(columnName = "sightmaplng", nullable = true, spOutputType = Types.VARCHAR, length = 50)
    private String sightmaplng;
    @DBColumn(columnName = "sightmaplat", nullable = true, spOutputType = Types.VARCHAR, length = 50)
    private String sightmaplat;
    @DBColumn(columnName = "sightdetail", nullable = true, spOutputType = Types.LONGNVARCHAR)
    private String sightdetail;
    @DBColumn(columnName = "sightopentime", nullable = true, spOutputType = Types.LONGNVARCHAR)
    private String sightopentime;
    @DBColumn(columnName = "sightreason", nullable = true, spOutputType = Types.LONGNVARCHAR)
    private String sightreason;
    @DBColumn(columnName = "commenturl", nullable = true, spOutputType = Types.VARCHAR, length = 500)
    private String commenturl;
    @DBColumn(columnName = "tipurl", nullable = true, spOutputType = Types.VARCHAR, length = 500)
    private String tipurl;
    @DBColumn(columnName = "tip", nullable = true, spOutputType = Types.LONGNVARCHAR)
    private String tip;
    @DBColumn(columnName = "ticketinfourl", nullable = true, spOutputType = Types.VARCHAR, length = 500)
    private String ticketinfourl;

    public String getTicketinfourl() {
        return ticketinfourl;
    }

    public void setTicketinfourl(String ticketinfourl) {
        this.ticketinfourl = ticketinfourl;
    }

    public String getSightreason() {
        return sightreason;
    }

    public void setSightreason(String sightreason) {
        this.sightreason = sightreason;
    }

    public String getCommenturl() {
        return commenturl;
    }

    public void setCommenturl(String commenturl) {
        this.commenturl = commenturl;
    }

    public String getTipurl() {
        return tipurl;
    }

    public void setTipurl(String tipurl) {
        this.tipurl = tipurl;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public long getTongchengresultid() {
        return id;
    }

    public void setTongchengresultid(long tongchengresultid) {
        this.id = tongchengresultid;
    }

    public long getSightid() {
        return sightid;
    }

    public void setSightid(long sightid) {
        this.sightid = sightid;
    }

    public String getSightname() {
        return sightname;
    }

    public void setSightname(String sightname) {
        this.sightname = sightname;
    }

    public String getSightlevel() {
        return sightlevel;
    }

    public void setSightlevel(String sightlevel) {
        this.sightlevel = sightlevel;
    }

    public String getSighturl() {
        return sighturl;
    }

    public void setSighturl(String sighturl) {
        this.sighturl = sighturl;
    }

    public String getSightticketprice() {
        return sightticketprice;
    }

    public void setSightticketprice(String sightticketprice) {
        this.sightticketprice = sightticketprice;
    }

    public Date getSpiderTime() {
        return spiderTime;
    }

    public void setSpiderTime(Date spiderTime) {
        this.spiderTime = spiderTime;
    }

    public long getTaskBuildTime() {
        return taskBuildTime;
    }

    public void setTaskBuildTime(long taskBuildTime) {
        this.taskBuildTime = taskBuildTime;
    }

    public String getSightfatherurl() {
        return sightfatherurl;
    }

    public void setSightfatherurl(String sightfatherurl) {
        this.sightfatherurl = sightfatherurl;
    }

    public String getSightaddress() {
        return sightaddress;
    }

    public void setSightaddress(String sightaddress) {
        this.sightaddress = sightaddress;
    }

    public String getSighttransport() {
        return sighttransport;
    }

    public void setSighttransport(String sighttransport) {
        this.sighttransport = sighttransport;
    }

    public String getSightmaplng() {
        return sightmaplng;
    }

    public void setSightmaplng(String sightmaplng) {
        this.sightmaplng = sightmaplng;
    }

    public String getSightmaplat() {
        return sightmaplat;
    }

    public void setSightmaplat(String sightmaplat) {
        this.sightmaplat = sightmaplat;
    }

    public String getSightdetail() {
        return sightdetail;
    }

    public void setSightdetail(String sightdetail) {
        this.sightdetail = sightdetail;
    }

    public String getSightopentime() {
        return sightopentime;
    }

    public void setSightopentime(String sightopentime) {
        this.sightopentime = sightopentime;
    }




}
