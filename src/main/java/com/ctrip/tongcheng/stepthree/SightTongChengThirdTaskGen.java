package com.ctrip.tongcheng.stepthree;

import com.ctrip.Utils.MySqlHelper;
import com.ctrip.common.Utils;
import com.ctrip.fx.enteroctopus.common.dao.DBHelper;
import com.ctrip.fx.enteroctopus.common.dao.DBHelperBuilder;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import com.ctrip.tongcheng.stepone.SightInfoTongChengFirstDecodeDetail;
import com.ctrip.tongcheng.steptwo.SightInfoTongChengSecondDecodeDetail;
import com.ctrip.tongcheng.steptwo.SightTongChengSecondTaskDetail;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import jxl.Sheet;
import jxl.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gcshen on 13-12-31.
 */
public class SightTongChengThirdTaskGen implements IGenerator {
    private static Logger log = LoggerFactory.getLogger(SightTongChengThirdTaskGen.class);
    public static long taskbuildtime = System.currentTimeMillis();
    //保存因为网络问题没有爬到页面的任务
    public  static List<SightTongChengThirdTaskDetail> task_surplus=new ArrayList<SightTongChengThirdTaskDetail>();
    //从DB中取最新数据
    List<SightTongChengThirdTaskDetail> taskList = getTasksFromDB();
    public  static int taskIndex = 0;

    @Override
    public Task genTask() {
        if (taskIndex == taskList.size()) {
            //log.info("第三步任务结束！");
            return null;
        }
        SightTongChengThirdTaskDetail taskDetail = taskList.get(taskIndex);
        taskIndex++;
        Task task = new Task();
        task.setId(taskIndex);
        task.setBuilder("SightTongChengThirdTest Builder");
        task.setBuildTime(taskbuildtime);
        task.setLastModify(System.currentTimeMillis());
        task.setTaskDetail(taskDetail);
        return task;

    }

    @Override
    public String genTaskStr() {
        return null;
    }

    private static List<SightTongChengThirdTaskDetail> getTasksFromDB() {
        List<SightTongChengThirdTaskDetail> tasks = new ArrayList<SightTongChengThirdTaskDetail>();
        List<SightInfoTongChengSecondDecodeDetail> secondresult = null;
        secondresult = MySqlHelper.LoadLatestEntities(SightInfoTongChengSecondDecodeDetail.class);
        for (int i = 0; i < secondresult.size(); i++) {
            SightTongChengThirdTaskDetail bean = new SightTongChengThirdTaskDetail();
            bean.setSightName(secondresult.get(i).getSightName());
            bean.setSightUrl(secondresult.get(i).getSightUrl());
            bean.setSightLevel(secondresult.get(i).getSightLevel());
            bean.setSightTicketPrice(secondresult.get(i).getSightTicketPrice());
            tasks.add(bean);
        }
        return tasks;
    }
}
