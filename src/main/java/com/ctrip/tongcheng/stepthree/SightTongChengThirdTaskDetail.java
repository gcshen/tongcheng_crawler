package com.ctrip.tongcheng.stepthree;

import com.ctrip.fx.enteroctopus.common.jpa.DBColumn;
import com.ctrip.fx.enteroctopus.common.jpa.DBEntity;
import com.ctrip.fx.enteroctopus.common.module.TaskDetail;

import java.sql.Types;

/**
 * Created by gcshen on 13-12-31.
 */
@DBEntity(tableName = "fx_octopus_task_detail_tongcheng_sight_third", useSP = true, dailyTable = false)
public class SightTongChengThirdTaskDetail extends TaskDetail {
    @DBColumn(columnName = "sightName", length = 20, spOutputType = Types.VARCHAR)
    private String sightName;
    @DBColumn(columnName = "sightUrl", length = 200, spOutputType = Types.VARCHAR)
    private String sightUrl;
    @DBColumn(columnName = "sightLevel", length = 6, spOutputType = Types.VARCHAR)
    private String sightLevel;
    @DBColumn(columnName = "sightTicketPrice", spOutputType = Types.INTEGER)
    private int sightTicketPrice;

    public String getSightName() {
        return sightName;
    }

    public void setSightName(String sightName) {
        this.sightName = sightName;
    }

    public String getSightUrl() {
        return sightUrl;
    }

    public void setSightUrl(String sightUrl) {
        this.sightUrl = sightUrl;
    }

    public String getSightLevel() {
        return sightLevel;
    }

    public void setSightLevel(String sightLevel) {
        this.sightLevel = sightLevel;
    }

    public int getSightTicketPrice() {
        return sightTicketPrice;
    }

    public void setSightTicketPrice(int sightTicketPrice) {
        this.sightTicketPrice = sightTicketPrice;
    }
}
