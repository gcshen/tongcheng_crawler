package com.ctrip.tongcheng.stepthree;

import com.ctrip.network.Command;
import com.ctrip.network.Executor;
import com.ctrip.network.Response;
import com.ctrip.fx.enteroctopus.common.util.NetworkUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by gcshen on 13-12-31.
 */
public class SightTongChengThirdParser {
    private static Logger log = LoggerFactory.getLogger(SightTongChengThirdParser.class);

    public SightInfoTongChengThirdDecodeDetail getResultDetail(Document htmlDocument, String url) {
        System.out.println("正在解析" + url);
        SightInfoTongChengThirdDecodeDetail thirdDecodeDetail = new SightInfoTongChengThirdDecodeDetail();
        String id = "", fatherUrl = "";
        String ticket_url = "http://www.17u.cn/scenery/AjaxHelper/SceneryPriceFrame.aspx?action=GETSPNEW&showCount=0&ids=#&isSimple=1&isShowAppThree=1&sceneryType=20301&widthtype=1&iid=0.9159196610562503";
        String tip_url = "http://www.17u.cn/scenery/AjaxHelper/SceneryPriceFrame.aspx?action=GetSceneryBookKnows&id=#&sceneryType=20301&iid=0.6705827875994146";
        String comment_url = "http://www.17u.cn/Scenery/AjaxHelper/AjaxCall.aspx?action=GetDPList&sort=1&mon=0&type=0&sId=#&page=1";
        try {
            id = htmlDocument.getElementById("hf_SceneryId").attr("value");
            fatherUrl = "http://17u.cn" + htmlDocument.getElementsByClass("hty_nav").first().getElementsByTag("a").last().attr("href");
            thirdDecodeDetail.setSightid(Integer.parseInt(id));
            thirdDecodeDetail.setSightfatherurl(fatherUrl);
            //url赋值
            String currentticketurl = ticket_url.replaceAll("#", Long.toString(thirdDecodeDetail.getSightid()));
            String currenttipurl = tip_url.replaceAll("#", Long.toString(thirdDecodeDetail.getSightid()));
            String currentcommenturl = comment_url.replaceAll("#", Long.toString(thirdDecodeDetail.getSightid()));
            thirdDecodeDetail.setSighturl(url);
            thirdDecodeDetail.setTicketinfourl(currentticketurl);
            thirdDecodeDetail.setTipurl(currenttipurl);
            thirdDecodeDetail.setCommenturl(currentcommenturl);
            //爬取时间
            thirdDecodeDetail.setSpiderTime(new Date());
            Elements all = htmlDocument.getAllElements();
            String sightname = htmlDocument.getElementsByTag("h1").first().text();
            thirdDecodeDetail.setSightname(sightname);
            //景点星级
            if (all.hasClass("scenicStar")) {
                String level = htmlDocument.getElementsByClass("scenicStar").first().text();
                thirdDecodeDetail.setSightlevel(level);
            }
            //地址
            if (all.hasClass("positionAddress")) {
                String address = htmlDocument.getElementsByClass("positionAddress").first().getElementsByTag("span").first().text();
                thirdDecodeDetail.setSightaddress(address);
            }
            //交通和百度地图坐标
            if (all.hasClass("traf_detail")) {
                String trans = htmlDocument.getElementsByClass("traf_detail").first().text();
                String maplng = htmlDocument.getElementById("mapLng").attr("value");
                String maplat = htmlDocument.getElementById("mapLat").attr("value");
                thirdDecodeDetail.setSighttransport(trans);
                thirdDecodeDetail.setSightmaplat(maplat);
                thirdDecodeDetail.setSightmaplng(maplng);
            }
            //游玩理由
            if (all.hasClass("reason_ul")) {
                String reason = "";
                Elements reasons = htmlDocument.getElementsByClass("reason_ul").first().getElementsByTag("li");
                for (int j = 0; j < reasons.size(); j++) {
                    if (reasons.get(j).children().size() > 1)
                        reason += reasons.get(j).getElementsByTag("span").first().text() + ":" + reasons.get(j).getElementsByTag("p").first().text() + "；";
                    else
                        reason += reasons.get(j).text();
                }
                thirdDecodeDetail.setSightreason(reason);
            }
            //门票信息
            Document doc_ticket = getResponse(currentticketurl, url);
            if (doc_ticket.getAllElements() != null) {
                Elements alltickets = doc_ticket.getAllElements();
                if (alltickets.hasClass("api-piao")) {
                    Element adult = doc_ticket.getElementsByClass("api-piao").select("div[ptypeid=60301]").first();
                    Elements adultlist = null;
                    if (adult != null && adult.getAllElements().hasClass("piao-list"))
                        adultlist = adult.getElementsByClass("piao-list");
                    Element child = doc_ticket.getElementsByClass("api-piao").select("div[ptypeid=60302]").first();
                    Element home = doc_ticket.getElementsByClass("api-piao").select("div[ptypeid=60307]").first();
                    Element recommend = doc_ticket.getElementsByClass("api-piao").select("div[ptypeid=-100]").first();
                    String ticket = "";
                    if (adult != null) {
                        for (int t = 0; t < adultlist.size(); t++)
                            if (adultlist.get(t).getElementsByClass("W01").first().getElementsByClass("name").first() != null)
                                ticket = adultlist.get(t).getElementsByClass("W01").first().getElementsByClass("name").first().text() + adult.getElementsByClass("W02").text() + "；";
                            else
                                ticket = adultlist.get(t).getElementsByClass("W01").first().getElementsByClass("phoneRed").text() + adultlist.get(t).getElementsByClass("W02").text() + "；";
                    }
                    if (child != null) {
                        if (child.getElementsByClass("W01").first().getElementsByClass("name") != null)
                            ticket += child.getElementsByClass("W01").first().getElementsByClass("name").first().text() + child.getElementsByClass("W02").text() + "；";
                        else
                            ticket = child.getElementsByClass("W01").first().getElementsByClass("phoneRed").text();
                    }
                    if (home != null) {
                        if (home.getElementsByClass("W01").first().getElementsByClass("name") != null)
                            ticket += home.getElementsByClass("W01").first().getElementsByClass("name").first().text() + home.getElementsByClass("W02").text() + "；";
                        else
                            ticket = home.getElementsByClass("W01").first().getElementsByClass("phoneRed").text();
                    }
                    if (adult == null && child == null && home == null && recommend != null) {
                        Elements recommendtickets = recommend.getElementsByClass("piao-list");

                        for (int k = 0; k < recommendtickets.size(); k++) {
                            ticket += recommendtickets.get(k).getElementsByClass("W01").text() + "：" + recommendtickets.get(k).getElementsByClass("W02").text() + "；";
                        }
                    }
                    thirdDecodeDetail.setSightticketprice(ticket);
                } else {
                    Elements ticket = htmlDocument.getElementsByClass("no_border");
                    if (ticket.size() > 0)
                        thirdDecodeDetail.setSightticketprice(ticket.first().text());
                }
            }
            //开放时间和建议
            Document doc_tip = getResponse(currenttipurl, url);
            if (doc_ticket.getAllElements() != null) {
                Elements tips = doc_tip.getAllElements();
                if (tips.hasClass("left_con_b")) {
                    Elements tipdetails = doc_tip.getElementsByClass("left_con_b").get(0).getElementsByClass("left_con_r_b");
                    String opentimehead = tipdetails.first().getElementsByClass("left_con_float").get(0).text();
                    if (!opentimehead.equals("1.开放时间:")) {
                        int count = doc_tip.getElementsByClass("left_con_b").size();
                        if (count > 1)
                            tipdetails = doc_tip.getElementsByClass("left_con_b").get(1).getElementsByClass("left_con_r_b");
                    }
                    thirdDecodeDetail.setSightopentime(tipdetails.first().getElementsByClass("left_con_float").get(1).text());
                    thirdDecodeDetail.setTip(tipdetails.last().getElementsByClass("left_con_float").get(1).text());
                }
            }
            //详细介绍
            if (all.hasClass("intro_information")) {
                String detail = "";
                Elements infos = htmlDocument.getElementsByClass("intro_information");
                for (int j = 0; j < infos.size(); j++) {
                    for (int k = 0; k < infos.get(j).children().size(); k++) {
                        detail += infos.get(j).child(k).text() + "，";
                    }
                }
                thirdDecodeDetail.setSightdetail(detail);
            } else {
                thirdDecodeDetail.setSightdetail(htmlDocument.getElementsByClass("left_con").first().text());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("=====Exception Info:" + ex.getMessage());
            log.info("解析此页中的元素时出错！" + url);
        }
        return thirdDecodeDetail;
    }

    private Document getResponse(String url, String reffer) {
        Document doc = null;
        Executor executor = new Executor();
        Command command = new Command();
        command.setUrl(url);
        command.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,**/;q=0.8");
        command.addHeader("Accept-Encoding", "gzip");
        command.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        command.addHeader("Connection", "keep-alive");
        command.addHeader("DNT", "1");
        command.addHeader("Host", "www.17u.cn");
        command.addHeader("Referer", reffer);
        command.addHeader("User-Agent", NetworkUtils.getNewerUserAgentrandomly());
        Response response = new Response();
        try {
            executor.executeGetGzip(command, response);
            String content = null;
            if (response.getResultCode() == Response.Success && response.getContent() != null) {
                content = new String(response.getContent());
            }
            if (content != null) {
                doc = Jsoup.parse(content);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info(e.getMessage());
            log.info("=======抓取当前页面出错:" + url);
            return null;
        }
        return doc;
    }
}

