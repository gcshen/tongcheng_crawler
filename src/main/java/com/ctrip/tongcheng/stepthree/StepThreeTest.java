package com.ctrip.tongcheng.stepthree;

import com.ctrip.Utils.MySqlHelper;
import com.ctrip.common.Utils;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.dao.DBHelper;
import com.ctrip.fx.enteroctopus.common.dao.DBHelperBuilder;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by gcshen on 13-12-31.
 */
public class StepThreeTest {
    public static void Test() {
        System.out.println("开始第三步：");
        Utils.loadConfig();
        //创建任务生成器和抓取器
        IGenerator tonghengThirdGenerator = new SightTongChengThirdTaskGen();
        ICoreWorker tongchengThirdCorWorker = new SightTongChengThirdCoreWorker();
        System.out.println(new Date());
        Task thirdTask;
        int count = 0;
        List<SightInfoTongChengThirdDecodeDetail> templist = new ArrayList<SightInfoTongChengThirdDecodeDetail>();
        while ((thirdTask = tonghengThirdGenerator.genTask()) != null) {
            Result result = tongchengThirdCorWorker.execute(thirdTask);
            if (result != null) {
                if (((List<SightInfoTongChengThirdDecodeDetail>) result.getResultDetail().getDataList()).size() > 0)
                    templist.add(((List<SightInfoTongChengThirdDecodeDetail>) result.getResultDetail().getDataList()).get(0));
                //设置批量入库参数
                if (templist.size() == 10) {
                    count++;
                    System.out.println("第三步，第" + count + "次写数据");
                    //存入数据库
                    MySqlHelper.SaveEntities(templist);
                    templist.clear();
                }
            }
        }
        //因为网络问题没有爬到页面的景点信息，最后执行一次
        for(int i=0;i<SightTongChengThirdTaskGen.task_surplus.size();i++)
        {
            SightTongChengThirdTaskDetail taskDetail = SightTongChengThirdTaskGen.task_surplus.get(i);
            Task task = new Task();
            task.setId(SightTongChengThirdTaskGen.taskIndex);
            task.setBuilder("SightTongChengThirdTest Builder");
            task.setBuildTime(SightTongChengThirdTaskGen.taskbuildtime);
            task.setLastModify(System.currentTimeMillis());
            task.setTaskDetail(taskDetail);
            Result result = tongchengThirdCorWorker.execute(task);
            if (result != null) {
                if (((List<SightInfoTongChengThirdDecodeDetail>) result.getResultDetail().getDataList()).size() > 0)
                    templist.add(((List<SightInfoTongChengThirdDecodeDetail>) result.getResultDetail().getDataList()).get(0));

            }
        }
        //最后不满入库参数的景点信息和设置批量入库参数和因为网络问题没有爬到页面的景点信息入库
        if (templist.size()!=0) {
            count++;
            System.out.println("第三步，第" + count + "次写数据，最后一批数据，");
            //存入数据库
            MySqlHelper.SaveEntities(templist);
            templist.clear();
        }
        System.out.println("第三步任务结束！");
    }
}
