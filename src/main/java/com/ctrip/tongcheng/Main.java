package com.ctrip.tongcheng;

import com.ctrip.tongcheng.stepone.StepOneTest;
import com.ctrip.tongcheng.stepthree.StepThreeTest;
import com.ctrip.tongcheng.steptwo.StepTwoTest;

/**
 * Created with IntelliJ IDEA.
 * User: yf_liu
 * Date: 13-11-15
 * Time: 下午1:44
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args) {
        //StepOneTest.Test();
        //StepTwoTest.Test();
        StepThreeTest.Test();
    }
}
