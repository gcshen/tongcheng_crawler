package com.ctrip.tongcheng.steptwo;

import com.ctrip.fx.enteroctopus.common.jpa.DBColumn;
import com.ctrip.fx.enteroctopus.common.jpa.DBEntity;
import com.ctrip.fx.enteroctopus.common.module.TaskDetail;

import java.sql.Types;

/**
 * Created by gcshen on 13-12-30.
 */
@DBEntity(tableName = "fx_octopus_task_detail_tongcheng_sight_second", useSP = true, dailyTable = false)
public class SightTongChengSecondTaskDetail extends TaskDetail {
    @DBColumn(columnName = "minId", spOutputType = Types.INTEGER)
    private int minId;
    @DBColumn(columnName = "maxId", spOutputType = Types.INTEGER)
    private int maxId;

    public void setMinId(int id) {
        this.minId = id;
    }

    public int getMinId() {
        return this.minId;
    }

    public void setMaxId(int id) {
        this.maxId = id;
    }

    public int getMaxId() {
        return this.maxId;
    }
}
