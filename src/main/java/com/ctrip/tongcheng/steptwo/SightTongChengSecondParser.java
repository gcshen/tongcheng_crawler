package com.ctrip.tongcheng.steptwo;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by gcshen on 13-12-30.
 */
public class SightTongChengSecondParser {
    private static Logger log = LoggerFactory.getLogger(SightTongChengSecondParser.class);

    public List<SightInfoTongChengSecondDecodeDetail> getResultDetail(
            Document htmlDocument, String url, long time) {
        List<SightInfoTongChengSecondDecodeDetail> ls = new ArrayList<SightInfoTongChengSecondDecodeDetail>();
        String name=null, level=null, price=null, sighturl=null;
        try {
            Elements ems = htmlDocument.getElementsByClass("info_top");
            for (Element e : ems) {
                SightInfoTongChengSecondDecodeDetail product = new SightInfoTongChengSecondDecodeDetail();
                ls.add(product);
                try {
                    product.setTaskBuildTime(time);
                    product.setSpiderTime(new Date());
                    Elements s=e.children();
                    name = (e.getElementsByClass("fir_name")).first().text();
                    product.setSightName(name);
                    sighturl = "http://17u.cn" + (e.getElementsByClass("fir_name")).first().attr("href");
                    product.setSightUrl(sighturl);
                    level = e.getElementsByClass("s_level").first().text();
                    product.setSightLevel(level);
                    if(s.hasClass("scenery_details"))
                    {
                        price = e.getElementsByClass("s_price").get(0).getElementsByTag("b").text();
                        product.setSightTicketPrice((price.isEmpty() ? 0 : Integer.parseInt(price)));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    log.info("解析此页中的项出错！" + url);
                    continue;
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.info("解析此页中的项时出错！" + url);
        }
        return ls;
    }
}
