package com.ctrip.tongcheng.steptwo;

import com.ctrip.common.ResultFactory;
import com.ctrip.network.Command;
import com.ctrip.network.Executor;
import com.ctrip.network.Response;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.fx.enteroctopus.common.util.NetworkUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by gcshen on 13-12-30.
 */
public class SightTongChengSecondCoreWorker implements ICoreWorker {
    private static Logger log = LoggerFactory.getLogger(SightTongChengSecondCoreWorker.class);
    String baseUrl = "http://www.17u.cn/scenery/SearchList.aspx?&action=getlist&page=pageIndex&kw=&pid=0&cid=0&cyid=0&theme=0&grade=0&money=0&sort=0&paytype=0&ismem=0&istuan=0&isnow=0&iid=0.08102992083877325";

    @Override
    public Result execute(Task task) {
        SightTongChengSecondResultDetail secondResultDetail = new SightTongChengSecondResultDetail();
        try {
            SightTongChengSecondTaskDetail taskDetail = (SightTongChengSecondTaskDetail) task.getTaskDetail();
            long taskBuildTime = task.getBuildTime();
            int beginIndex = taskDetail.getMinId();
            int endIndex = taskDetail.getMaxId();
            String currentUrl;
            Document document;
            SightTongChengSecondParser SightTongChengFirstParser = new SightTongChengSecondParser();
            List<SightInfoTongChengSecondDecodeDetail> secondDecodeDetail = null;
            //测试起始页
            for (int i = beginIndex; i <= endIndex; i++) {
                try {
                    log.info("正在解析第"+i+"页");
                    currentUrl="";
                    document=null;
                    secondDecodeDetail=null;
                    currentUrl = baseUrl.replaceFirst("pageIndex", Integer.toString(i));
                    document = getResponse(currentUrl);
                    if (document == null) {
                        System.out.println("没有获取页面："+currentUrl);
                    } else {
                        secondDecodeDetail = SightTongChengFirstParser.getResultDetail(document, currentUrl, taskBuildTime);
                        secondResultDetail.getDataList().addAll(secondDecodeDetail);
                    }
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                    log.info("第"+i+"页数据有缺项！");
                    continue;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("============Exception Info:" + e.getMessage());
            log.info("第二步执行同程景点数抓取失败！");
        }

        ResultFactory rf=ResultFactory.instance();
        Result secondTaskResult=rf.getResult(task);
        secondTaskResult.setTask(task);
        secondTaskResult.setResultDetail(secondResultDetail);
        return secondTaskResult;
    }

    private Document getResponse(String url) {
        Document doc = null;
        Executor executor = new Executor();
        Command command = new Command();
        command.setUrl(url);
        command.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,**/;q=0.8");
        command.addHeader("Accept-Encoding", "gzip");
        command.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        command.addHeader("Connection", "keep-alive");
        command.addHeader("DNT", "1");
        command.addHeader("Host", "www.17u.cn");
        command.addHeader("Referer", "http://www.17u.cn/scenery/scenerysearchlist_0_0__0_0_0.html");
        command.addHeader("User-Agent", NetworkUtils.getNewerUserAgentrandomly());
        Response response = new Response();
        try {
            executor.executeGetGzip(command, response);
            String content = null;
            if (response.getResultCode() == Response.Success && response.getContent() != null) {
                content = new String(response.getContent());
            }
            if (content != null) {
                doc = Jsoup.parse(content);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("=======抓取当前页面出错:" + url);
            return null;
        }
        return doc;
    }
}
