package com.ctrip.tongcheng.steptwo;

import com.ctrip.Utils.MySqlHelper;
import com.ctrip.common.Utils;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.dao.DBHelper;
import com.ctrip.fx.enteroctopus.common.dao.DBHelperBuilder;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by gcshen on 13-12-30.
 */

public class StepTwoTest {
    public static void Test() {
        System.out.println("开始第二步：");
        Utils.loadConfig();
        //创建任务生成器和抓取器
        IGenerator tonghengSecondGenerator = new SightTongChengSecondTaskGen();
        ICoreWorker tongchengSecondCorWorker = new SightTongChengSecondCoreWorker();
        System.out.println(new Date());
        Task secondTask;
        int count=0;
        //入数据库，每个任务携带一10页，100条数据
        while ((secondTask = tonghengSecondGenerator.genTask()) != null) {
            Result result = tongchengSecondCorWorker.execute(secondTask);
            if (result != null) {
                count++;
                System.out.println("第二步，第"+count+"次写数据");
                MySqlHelper.SaveEntities(result.getResultDetail().getDataList());
            }
        }
    }
}
