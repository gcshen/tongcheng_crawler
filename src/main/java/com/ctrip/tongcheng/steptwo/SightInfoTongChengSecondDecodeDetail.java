package com.ctrip.tongcheng.steptwo;

import com.ctrip.fx.enteroctopus.common.jpa.DBColumn;
import com.ctrip.fx.enteroctopus.common.jpa.DBEntity;
import com.ctrip.fx.enteroctopus.common.jpa.DBId;

import java.sql.Types;
import java.util.Date;

/**
 * Created by gcshen on 13-12-30.
 */
@DBEntity(tableName = "fx_octopus_result_detail_sight_tongcheng_second")
public class SightInfoTongChengSecondDecodeDetail {
    @DBId
    @DBColumn(spParamIndex = 2, spOutput = true, spOutputType = Types.BIGINT)
    private long id;
    @DBColumn(columnName="sightName",nullable = true,length = 100,spOutputType = Types.VARCHAR)
    private String sightName;
    @DBColumn(columnName="sightUrl",nullable = true,length = 100,spOutputType = Types.VARCHAR)
    private String sightUrl;
    @DBColumn(columnName="sightLevel",nullable = true,length = 6,spOutputType = Types.CHAR)
    private String sightLevel;
    @DBColumn(columnName="sightTicketPrice",nullable = true,spOutputType = Types.INTEGER)
    private int sightTicketPrice;
    @DBColumn(columnName="spiderTime",nullable = true,spOutputType = Types.DATE)
    private Date spiderTime;
    @DBColumn(columnName="taskBuildTime",nullable = true,spOutputType = Types.BIGINT)
    private long taskBuildTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSightName() {
        return sightName;
    }

    public void setSightName(String sightName) {
        this.sightName = sightName;
    }

    public String getSightUrl() {
        return sightUrl;
    }

    public void setSightUrl(String sightUrl) {
        this.sightUrl = sightUrl;
    }

    public String getSightLevel() {
        return sightLevel;
    }

    public void setSightLevel(String sightLevel) {
        this.sightLevel = sightLevel;
    }

    public int getSightTicketPrice() {
        return sightTicketPrice;
    }

    public void setSightTicketPrice(int sightTicketPrice) {
        this.sightTicketPrice = sightTicketPrice;
    }

    public Date getSpiderTime() {
        return spiderTime;
    }

    public void setSpiderTime(Date spiderTime) {
        this.spiderTime = spiderTime;
    }

    public long getTaskBuildTime() {
        return taskBuildTime;
    }

    public void setTaskBuildTime(long taskBuildTime) {
        this.taskBuildTime = taskBuildTime;
    }
}
