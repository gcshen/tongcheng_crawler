package com.ctrip.tongcheng.steptwo;

import com.ctrip.Utils.MySqlHelper;
import com.ctrip.common.Utils;
import com.ctrip.fx.enteroctopus.common.dao.DBHelper;
import com.ctrip.fx.enteroctopus.common.dao.DBHelperBuilder;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import com.ctrip.tongcheng.stepone.SightInfoTongChengFirstDecodeDetail;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import jxl.Sheet;
import jxl.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gcshen on 13-12-30.
 */
public class SightTongChengSecondTaskGen implements IGenerator {
    private static Logger log = LoggerFactory.getLogger(SightTongChengSecondCoreWorker.class);
    long taskbuildtime = System.currentTimeMillis();
    //从DB中取最新数据
    List<SightTongChengSecondTaskDetail> taskList = getTasksFromDB();
    private static int taskIndex = 0;

    @Override
    public Task genTask() {

        if (taskIndex == taskList.size()) {
            log.info("第二步任务结束！");
            return null;
        }
        SightTongChengSecondTaskDetail taskDetail = taskList.get(taskIndex);
        taskIndex++;
        Task task = new Task();
        task.setId(2);
        task.setBuilder("SightTongChengScondTest Builder");
        task.setBuildTime(taskbuildtime);
        task.setLastModify(System.currentTimeMillis());
        task.setTaskDetail(taskDetail);
        return task;
    }

    @Override
    public String genTaskStr() {
        return null;
    }

    private static List<SightTongChengSecondTaskDetail> getTasksFromDB() {
        List<SightTongChengSecondTaskDetail> tasks = new ArrayList<SightTongChengSecondTaskDetail>();
        List<SightInfoTongChengFirstDecodeDetail> firstresult = null;
        firstresult = MySqlHelper.LoadLatestEntities(SightInfoTongChengFirstDecodeDetail.class);
        for (int i = 0; i < firstresult.size(); i++) {
            SightTongChengSecondTaskDetail bean = new SightTongChengSecondTaskDetail();
            bean.setMinId(firstresult.get(i).getBeginPageIndex());
            bean.setMaxId(firstresult.get(i).getEndPageIndex());
            tasks.add(bean);
        }
        return tasks;
    }
}

