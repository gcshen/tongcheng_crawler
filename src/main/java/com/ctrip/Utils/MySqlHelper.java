package com.ctrip.Utils;

import com.ctrip.fx.enteroctopus.common.dao.DBHelper;
import com.ctrip.fx.enteroctopus.common.dao.DBHelperBuilder;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import java.util.List;

/**
 * Created by gcshen on 14-2-19.
 */
public class MySqlHelper {
    public static DBHelper dbHelper;
    public static void SetDBHelper()
    {
        String url = "jdbc:mysql://localhost:3306/crawler?user=root&password=0";
        MysqlDataSource datasource = new MysqlDataSource();
        datasource.setUrl(url);
        dbHelper = DBHelperBuilder.getInstance().init(datasource).getDbHelper();
    }
    public static void  SaveEntities(List<?> objects)
    {
        SetDBHelper();
        dbHelper.saveEntities(objects);
    }
    public static  <T>  List<T> LoadLatestEntities(Class<T> tclass)
    {
        SetDBHelper();
        return dbHelper.loadLatest(tclass);
    }
}
