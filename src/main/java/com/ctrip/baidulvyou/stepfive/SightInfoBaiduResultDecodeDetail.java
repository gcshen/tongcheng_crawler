package com.ctrip.baidulvyou.stepfive;

import java.util.Date;

/**
 * Created by gcshen on 14-1-7.
 */
public class SightInfoBaiduResultDecodeDetail {
    private int id;
    private String sightname;
    private String sightid;
    private String sightaddress;
    private String sightopentime;
    private String sightticketprice;
    private String phone;
    private String description;
    private String transport;
    private String level;
    private String travelduration;
    private String website;
    private Date spidertime;
    private long taskbuildtime;
    private String mapx;
    private String mapy;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSightname() {
        return sightname;
    }

    public void setSightname(String sightname) {
        this.sightname = sightname;
    }

    public String getSightid() {
        return sightid;
    }

    public void setSightid(String sightid) {
        this.sightid = sightid;
    }

    public String getSightaddress() {
        return sightaddress;
    }

    public void setSightaddress(String sightaddress) {
        this.sightaddress = sightaddress;
    }

    public String getSightopentime() {
        return sightopentime;
    }

    public void setSightopentime(String sightopentime) {
        this.sightopentime = sightopentime;
    }

    public String getSightticketprice() {
        return sightticketprice;
    }

    public void setSightticketprice(String sightticketprice) {
        this.sightticketprice = sightticketprice;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransport() {
        return transport;
    }

    public void setTransport(String transport) {
        this.transport = transport;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTravelduration() {
        return travelduration;
    }

    public void setTravelduration(String travelduration) {
        this.travelduration = travelduration;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public Date getSpidertime() {
        return spidertime;
    }

    public void setSpidertime(Date spidertime) {
        this.spidertime = spidertime;
    }

    public long getTaskbuildtime() {
        return taskbuildtime;
    }

    public void setTaskbuildtime(long taskbuildtime) {
        this.taskbuildtime = taskbuildtime;
    }

    public String getMapx() {
        return mapx;
    }

    public void setMapx(String mapx) {
        this.mapx = mapx;
    }

    public String getMapy() {
        return mapy;
    }

    public void setMapy(String mapy) {
        this.mapy = mapy;
    }
}
