package com.ctrip.baidulvyou.stepfive;

import com.ctrip.common.Utils;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by gcshen on 14-1-7.
 */
public class StepFiveTest {
    public static void Test() {
        Utils.loadConfig();

        //创建任务生成器和抓取器
        IGenerator baiduFifthGenerator = new SightBaiduFifthTaskGen();
        ICoreWorker baiduFifthCorWorker = new SightBaiduFifthCoreWorker();
        System.out.println(new Date());
        Task FifthTask;
        //i用来控制循环次数,测试用
        int i=0;
        while ((FifthTask = baiduFifthGenerator.genTask()) != null) {
            Result result = baiduFifthCorWorker.execute(FifthTask);
            if (result != null) {
                //测试生成excel
                    genExcel(((SightBaiduFifthResultDetail) result.getResultDetail()).getDataList());
            }
            //i++;
        }
    }

    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("id")));
            sheet.addCell(new Label(1, 0, String.valueOf("sightname")));
            sheet.addCell(new Label(2, 0, String.valueOf("sightid")));
            sheet.addCell(new Label(3, 0, String.valueOf("sightaddress")));
            sheet.addCell(new Label(4, 0, String.valueOf("sightopentime")));
            sheet.addCell(new Label(5, 0, String.valueOf("sightticketprice")));
            sheet.addCell(new Label(6, 0, String.valueOf("phone")));
            sheet.addCell(new Label(7, 0, String.valueOf("description")));
            sheet.addCell(new Label(8, 0, String.valueOf("transport")));
            sheet.addCell(new Label(9, 0, String.valueOf("level")));
            sheet.addCell(new Label(10, 0, String.valueOf("travelduration")));
            sheet.addCell(new Label(11, 0, String.valueOf("website")));
            sheet.addCell(new Label(12, 0, String.valueOf("spidertime")));
            sheet.addCell(new Label(13, 0, String.valueOf("taskbuildtime")));
            sheet.addCell(new Label(14, 0, String.valueOf("mapx")));
            sheet.addCell(new Label(15, 0, String.valueOf("mapy")));

        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(List<SightInfoBaiduResultDecodeDetail> hotels) {
        File os = null;
        String excelPath = Utils.getConfigPath() + "result" + File.separator + "SightInfoBaiduReslutDecodeDetail.xls";
        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取结果", 0);
            }
            excelHeaders(sheet);
            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < hotels.size(); i++) {
                        SightInfoBaiduResultDecodeDetail hotel = hotels
                                .get(i);
                        sheet.addCell(new Label(0, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getId())));
                        sheet.addCell(new Label(1, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getSightname())));
                        sheet.addCell(new Label(2, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getSightid())));
                        sheet.addCell(new Label(3, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getSightaddress())));
                        sheet.addCell(new Label(4, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getSightopentime())));
                        sheet.addCell(new Label(5, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getSightticketprice())));
                        sheet.addCell(new Label(6, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getPhone())));
                        sheet.addCell(new Label(7, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getDescription())));
                        sheet.addCell(new Label(8, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getTransport())));
                        sheet.addCell(new Label(9, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getLevel())));
                        sheet.addCell(new Label(10, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getTravelduration())));
                        sheet.addCell(new Label(11, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getWebsite())));
                        sheet.addCell(new Label(12, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getSpidertime())));
                        sheet.addCell(new Label(13, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getTaskbuildtime())));
                        sheet.addCell(new Label(14, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getMapx())));
                        sheet.addCell(new Label(15, rwb.getSheet(0).getRows()
                                + i, String.valueOf(hotel.getMapy())));
                    }
                } else {
                    for (int i = 0; i < hotels.size(); i++) {
                        SightInfoBaiduResultDecodeDetail hotel = hotels
                                .get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(hotel.getId())));
                        sheet.addCell(new Label(1, 1 + i, String.valueOf(hotel.getSightname())));
                        sheet.addCell(new Label(2, 1 + i, String.valueOf(hotel.getSightid())));
                        sheet.addCell(new Label(3, 1 + i, String.valueOf(hotel.getSightaddress())));
                        sheet.addCell(new Label(4, 1 + i, String.valueOf(hotel.getSightopentime())));
                        sheet.addCell(new Label(5, 1 + i, String.valueOf(hotel.getSightticketprice())));
                        sheet.addCell(new Label(6, 1 + i, String.valueOf(hotel.getPhone())));
                        sheet.addCell(new Label(7, 1 + i, String.valueOf(hotel.getDescription())));
                        sheet.addCell(new Label(8, 1 + i, String.valueOf(hotel.getTransport())));
                        sheet.addCell(new Label(9, 1 + i, String.valueOf(hotel.getLevel())));
                        sheet.addCell(new Label(10, 1 + i, String.valueOf(hotel.getTravelduration())));
                        sheet.addCell(new Label(11, 1 + i, String.valueOf(hotel.getWebsite())));
                        sheet.addCell(new Label(12, 1 + i, String.valueOf(hotel.getSpidertime())));
                        sheet.addCell(new Label(13, 1 + i, String.valueOf(hotel.getTaskbuildtime())));
                        sheet.addCell(new Label(14, 1 + i, String.valueOf(hotel.getMapx())));
                        sheet.addCell(new Label(15, 1 + i, String.valueOf(hotel.getMapy())));


                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

}
