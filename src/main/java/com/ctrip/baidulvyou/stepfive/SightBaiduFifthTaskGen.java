package com.ctrip.baidulvyou.stepfive;

import com.ctrip.baidulvyou.stepone.SightBaiduFirstTaskDetail;
import com.ctrip.common.Utils;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import jxl.Sheet;
import jxl.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by gcshen on 14-1-7.
 */
public class SightBaiduFifthTaskGen implements IGenerator {
    private static List<SightBaiduFifthTaskDetail> taskList = getTasks();
    private static int taskIndex = 0;
    private static Logger log = LoggerFactory.getLogger(SightBaiduFifthTaskGen.class);

    @Override
    public Task genTask() {
        if (taskIndex == taskList.size()) {
            log.info("第五步任务完成！");
            return null;
        }
        SightBaiduFifthTaskDetail taskDetail = taskList.get(taskIndex);
        taskIndex++;
        Task task = new Task();
        task.setId(taskIndex);
        task.setBuilder("SightBaiduFirstTest Builder");
        task.setBuildTime(taskDetail.getTaskbuildtime());
        task.setLastModify(System.currentTimeMillis());
        task.setTaskDetail(taskDetail);
        return task;
    }

    @Override
    public String genTaskStr() {
        return null;
    }

    private static List<SightBaiduFifthTaskDetail> getTasks() {
        List<SightBaiduFifthTaskDetail> tasks = new ArrayList<SightBaiduFifthTaskDetail>();
        //获取LayerThree excel表中的数据
        String url = "http://lvyou.baidu.com/destination/ajax/allview?surl=#&format=ajax&cid=0&pn=1";
        SightBaiduFirstTaskDetail taskDetail = new SightBaiduFirstTaskDetail();
        File os = null;
        String[] excelsPath = {Utils.getConfigPath() + "result" + File.separator + "SightInfoBaiduLayerFiveDecodeDetail.xls",
                Utils.getConfigPath() + "result" + File.separator + "SightInfoBaiduLayerSixDecodeDetail.xls"};

        try {
            for (int j = 0; j < excelsPath.length;j++) {
                os = new File(excelsPath[j]);
                Workbook rwb = null;
                Sheet sheet;
                if (os.exists()) {
                    rwb = Workbook.getWorkbook(os);
                    sheet = rwb.getSheet("抓取百度旅游结果");
                } else {
                    log.info("第一步的Excel表没生成或缺少表！");
                    return null;
                }
                int rowNumber = sheet.getRows();
                String currenturl;
                for (int i = 1; i < rowNumber; i++) {
                    SightBaiduFifthTaskDetail bean = new SightBaiduFifthTaskDetail();
                    String name = sheet.getColumn(4)[i].getContents();
                    if (name.equals("null")) {
                        bean.setName(sheet.getColumn(1)[i].getContents());
                        currenturl = url.replaceAll("#", sheet.getColumn(1)[i].getContents());
                    } else {
                        bean.setName(sheet.getColumn(4)[i].getContents());
                        currenturl = url.replaceAll("#", sheet.getColumn(4)[i].getContents());
                    }
                    bean.setUrl(currenturl);
                    bean.setTaskbuildtime(Long.parseLong(sheet.getColumn(9)[i].getContents()));
                    tasks.add(bean);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        HashSet<SightBaiduFifthTaskDetail> h = new HashSet<SightBaiduFifthTaskDetail>(tasks);
        tasks.clear();
        tasks.addAll(h);
        return tasks;
    }
}
