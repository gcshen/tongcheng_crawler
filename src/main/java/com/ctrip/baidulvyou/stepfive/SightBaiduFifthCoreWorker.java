package com.ctrip.baidulvyou.stepfive;

import com.ctrip.common.ResultFactory;
import com.ctrip.network.Command;
import com.ctrip.network.Executor;
import com.ctrip.network.Response;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.fx.enteroctopus.common.util.NetworkUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by gcshen on 14-1-7.
 */
public class SightBaiduFifthCoreWorker implements ICoreWorker {
    private static Logger log = LoggerFactory.getLogger(SightBaiduFifthCoreWorker.class);

    @Override
    public Result execute(Task task) {
        ResultFactory rf = ResultFactory.instance();
        Result FifthTaskResult = rf.getResult(task);
        SightBaiduFifthResultDetail FifthResultDetail = new SightBaiduFifthResultDetail();
        String baseUrl = "http://lvyou.baidu.com/destination/ajax/allview?surl=#&format=ajax&cid=0&pn=@";
        try {
            SightBaiduFifthTaskDetail taskDetail = (SightBaiduFifthTaskDetail) task.getTaskDetail();
            long taskBuildTime = task.getBuildTime();
            String currentname = taskDetail.getName();
            String url = baseUrl.replaceAll("#", currentname);
            //抓取Json数据
            JSONObject jsonresult = getResponse(url);
            if(jsonresult==null)
            {
                log.info("第五步获取"+url+"json数据出错！");
                return null;
            }
            JSONObject jdata = jsonresult.getJSONObject("data");
            SightInfoBaiduResultDecodeDetail firstResultDecodeDetail = new SightInfoBaiduResultDecodeDetail();
            firstResultDecodeDetail.setTaskbuildtime(taskBuildTime);
            firstResultDecodeDetail.setSightname(jdata.getString("sname"));
            firstResultDecodeDetail.setSightid(jdata.getString("surl"));
            String address = jdata.getJSONObject("ext").getString("address");
            String phone = jdata.getJSONObject("ext").getString("phone");
            String level = jdata.getJSONObject("ext").getString("level");
            String description = jdata.getJSONObject("ext").getString("more_desc");
            String mapx = jdata.getJSONObject("ext").getString("map_x");
            String mapy = jdata.getJSONObject("ext").getString("map_y");
            firstResultDecodeDetail.setSightaddress(address);
            firstResultDecodeDetail.setPhone(phone);
            firstResultDecodeDetail.setDescription(description);
            firstResultDecodeDetail.setMapx(mapx);
            firstResultDecodeDetail.setMapy(mapy);
            firstResultDecodeDetail.setLevel(level);
            firstResultDecodeDetail.setWebsite(jdata.getJSONObject("ext").getString("website"));
            firstResultDecodeDetail.setSpidertime(new Date());
            try {
                JSONObject content = jdata.getJSONObject("content");
                firstResultDecodeDetail.setSightopentime(content.getJSONObject("ticket_info").getString("open_time_desc"));
                firstResultDecodeDetail.setSightticketprice(content.getJSONObject("ticket_info").getString("price_desc"));
                firstResultDecodeDetail.setTransport(content.getJSONObject("traffic").toString());
                firstResultDecodeDetail.setTravelduration(content.getJSONObject("besttime").toString());
            } catch (Exception e) {
                e.printStackTrace();
                log.info("景点"+currentname+"没有门票、开放时间、交通或者推荐游玩时间信息！");
            }
            FifthResultDetail.getDataList().add(firstResultDecodeDetail);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("Json数据缺少某一项！");
        }
        FifthTaskResult.setTask(task);
        FifthTaskResult.setResultDetail(FifthResultDetail);
        return FifthTaskResult;
    }

    private JSONObject getResponse(String url) {
        JSONObject json = null;
        Executor executor = new Executor();
        Command command = new Command();
        command.setUrl(url);
        command.addHeader("Accept", "text/html");
        command.addHeader("Accept-Encoding", "gzip");
        command.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        command.addHeader("Connection", "keep-alive");
        command.addHeader("DNT", "1");
        command.addHeader("Host", "lvyou.baidu.com");
        command.addHeader("Referer", "http://lvyou.baidu.com");
        command.addHeader("User-Agent", NetworkUtils.getNewerUserAgentrandomly());
        Response response = new Response();
        executor.executeGetGzip(command, response);
        String content = null;
        if (response.getResultCode() == Response.Success && response.getContent() != null) {
            content = new String(response.getContent());
        } else {
            log.info("第五步获取"+url+"Json数据出错！");
            return null;
        }
        if (content != null) {
            try {
                json = new JSONObject(content.substring(content.indexOf("{"), content.lastIndexOf("}") + 1));
                return json;
            } catch (Exception e) {
                e.printStackTrace();
                log.info("第五步获取"+url+"Json数据出错！");
            }
        }
        return null;
    }
}
