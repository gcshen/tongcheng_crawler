package com.ctrip.baidulvyou.stepthree;

import com.ctrip.baidulvyou.stepone.SightInfoBaiduLayerDecodeDetail;
import com.ctrip.common.Utils;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * Created by gcshen on 14-1-7.
 */
public class StepThreeTest {
    public static void Test() {
        Utils.loadConfig();

        //创建任务生成器和抓取器
        IGenerator baiduThirdGenerator = new SightBaiduThirdTaskGen();
        ICoreWorker baiduThirdCorWorker = new SightBaiduThirdCoreWorker();
        System.out.println(new Date());
        Task ThirdTask;
        //i用来控制循环次数,测试用
        int i=0;
        while ((ThirdTask = baiduThirdGenerator.genTask()) != null) {
            Result result = baiduThirdCorWorker.execute(ThirdTask);
            if (result != null) {
                //测试生成excel,根据不同层级生成不同层级表
                if(((SightBaiduThirdResultDetail) result.getResultDetail()).getLayerFiveList().size()!=0)
                    genExcel(((SightBaiduThirdResultDetail) result.getResultDetail()).getLayerFiveList(), 5);
                if(((SightBaiduThirdResultDetail) result.getResultDetail()).getLayerSixList().size()!=0)
                    genExcel(((SightBaiduThirdResultDetail) result.getResultDetail()).getLayerSixList(), 6);
            }
            //i++;
        }
    }
    synchronized private static void excelHeaders(WritableSheet sheet) {
        try {
            sheet.addCell(new Label(0, 0, String.valueOf("id")));
            sheet.addCell(new Label(1, 0, String.valueOf("name")));
            sheet.addCell(new Label(2, 0, String.valueOf("url")));
            sheet.addCell(new Label(3, 0, String.valueOf("scene_layer")));
            sheet.addCell(new Label(4, 0, String.valueOf("en_name")));
            sheet.addCell(new Label(5, 0, String.valueOf("map_x")));
            sheet.addCell(new Label(6, 0, String.valueOf("map_y")));
            sheet.addCell(new Label(7, 0, String.valueOf("description")));
            sheet.addCell(new Label(8, 0, String.valueOf("sipdertime")));
            sheet.addCell(new Label(9, 0, String.valueOf("taskbuildtime")));
        } catch (RowsExceededException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

    synchronized public static void genExcel(List<SightInfoBaiduLayerDecodeDetail> sights, int layer) {
        File os = null;
        String excelDir = Utils.getConfigPath() + "result";
        String excelPath = null;
        switch (layer) {
            case 3:
                excelPath = Utils.getConfigPath() + "result" + File.separator + "SightInfoBaiduLayerThreeDecodeDetail.xls";
                break;
            case 4:
                excelPath = Utils.getConfigPath() + "result" + File.separator + "SightInfoBaiduLayerFourDecodeDetail.xls";
                break;
            case 5:
                excelPath = Utils.getConfigPath() + "result" + File.separator + "SightInfoBaiduLayerFiveDecodeDetail.xls";
                break;
            case 6:
                excelPath = Utils.getConfigPath() + "result" + File.separator + "SightInfoBaiduLayerSixDecodeDetail.xls";
                break;
        }
        File f = new File(excelDir);
        if (!f.isDirectory()) {
            f.mkdirs();
        }

        try {
            os = new File(excelPath);
            WritableWorkbook wb = null;
            Workbook rwb = null;
            if (os.exists()) {
                rwb = Workbook.getWorkbook(os);
            }
            wb = Workbook.createWorkbook(os);
            WritableSheet sheet = wb.getSheet("抓取百度旅游结果");

            if (sheet == null) {
                sheet = wb.createSheet("抓取百度旅游结果", 0);
            }
            excelHeaders(sheet);

            try {
                if (rwb != null) {
                    for (int row = 0; row < rwb.getSheet(0).getRows(); row++) {
                        for (int col = 0; col < rwb.getSheet(0).getColumns(); col++) {
                            sheet.addCell(new Label(col, row, rwb.getSheet(0).getCell(col, row).getContents()));
                        }
                    }
                    for (int i = 0; i < sights.size(); i++) {
                        SightInfoBaiduLayerDecodeDetail sight = sights
                                .get(i);
                        sheet.addCell(new Label(0, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getId())));
                        sheet.addCell(new Label(1, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(2, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getUrl())));
                        sheet.addCell(new Label(3, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getScene_layer())));
                        sheet.addCell(new Label(4, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getEn_name())));
                        sheet.addCell(new Label(5, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getMap_x())));
                        sheet.addCell(new Label(6, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getMap_y())));
                        sheet.addCell(new Label(7, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getDescription())));
                        sheet.addCell(new Label(8, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getSpidertime())));
                        sheet.addCell(new Label(9, rwb.getSheet(0).getRows()
                                + i, String.valueOf(sight.getTaskbuildtime())));
                    }
                } else {
                    for (int i = 0; i < sights.size(); i++) {
                        SightInfoBaiduLayerDecodeDetail sight = sights
                                .get(i);
                        sheet.addCell(new Label(0, 1 + i, String.valueOf(sight.getId())));
                        sheet.addCell(new Label(1, 1 + i, String.valueOf(sight.getName())));
                        sheet.addCell(new Label(2, 1 + i, String.valueOf(sight.getUrl())));
                        sheet.addCell(new Label(3, 1 + i, String.valueOf(sight.getScene_layer())));
                        sheet.addCell(new Label(4, 1 + i, String.valueOf(sight.getEn_name())));
                        sheet.addCell(new Label(5, 1 + i, String.valueOf(sight.getMap_x())));
                        sheet.addCell(new Label(6, 1 + i, String.valueOf(sight.getMap_y())));
                        sheet.addCell(new Label(7, 1 + i, String.valueOf(sight.getDescription())));
                        sheet.addCell(new Label(8, 1 + i, String.valueOf(sight.getSpidertime())));
                        sheet.addCell(new Label(9, 1 + i, String.valueOf(sight.getTaskbuildtime())));
                    }
                }
            } catch (RowsExceededException e) {
                e.printStackTrace();
            } catch (WriteException e) {
                e.printStackTrace();
            }
            wb.write();
            wb.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (WriteException e1) {
            e1.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }
}
