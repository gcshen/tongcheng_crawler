package com.ctrip.baidulvyou.stepfour;

import com.ctrip.baidulvyou.stepone.SightInfoBaiduLayerDecodeDetail;
import com.ctrip.common.ResultFactory;
import com.ctrip.network.Command;
import com.ctrip.network.Executor;
import com.ctrip.network.Response;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.fx.enteroctopus.common.util.NetworkUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by gcshen on 14-1-7.
 */
public class SightBaiduFourthCoreWorker implements ICoreWorker {
    private static Logger log = LoggerFactory.getLogger(SightBaiduFourthCoreWorker.class);

    @Override
    public Result execute(Task task) {
        ResultFactory rf = ResultFactory.instance();
        Result FourthTaskResult = rf.getResult(task);
        SightBaiduFourthResultDetail FourthResultDetail = new SightBaiduFourthResultDetail();
        String baseUrl = "http://lvyou.baidu.com/destination/ajax/allview?surl=#&format=ajax&cid=0&pn=@";
        try {
            SightBaiduFourthTaskDetail taskDetail = (SightBaiduFourthTaskDetail) task.getTaskDetail();
            long taskBuildTime = task.getBuildTime();
            String url = taskDetail.getUrl();
            //抓取Json数据
            JSONObject jsonresult = getResponse(url);
            JSONObject jdata;
            int scene_total;
            int page_count;
            //解析第一页Json
            if (jsonresult.getInt("errno") != 0) {
                log.info("第四步第一页链接" + taskDetail.getUrl() + "没有Json数据！");
                return null;
            } else {
                jdata = jsonresult.getJSONObject("data");
                scene_total = jdata.getInt("scene_total");
                if (scene_total == 0) {
                    return null;
                }
                JSONArray jsonArray = jdata.getJSONArray("scene_list");
                if (scene_total / jsonArray.length() >= 1 && scene_total != jsonArray.length())
                    page_count = scene_total / jsonArray.length() + 1;
                else
                    page_count = 1;
                for (int i = 0; i < jsonArray.length(); i++) {
                    SightInfoBaiduLayerDecodeDetail FourthDecodeDetail = new SightInfoBaiduLayerDecodeDetail();
                    FourthDecodeDetail.setName(jsonArray.getJSONObject(i).getString("sname"));
                    FourthDecodeDetail.setUrl("lvyou.baidu.com/" + jsonArray.getJSONObject(i).getString("surl"));
                    FourthDecodeDetail.setEn_name(jsonArray.getJSONObject(i).getString("surl"));
                    FourthDecodeDetail.setScene_layer(jsonArray.getJSONObject(i).getInt("scene_layer"));
                    FourthDecodeDetail.setMap_x(jsonArray.getJSONObject(i).getJSONObject("ext").getString("map_x"));
                    FourthDecodeDetail.setMap_y(jsonArray.getJSONObject(i).getJSONObject("ext").getString("map_y"));
                    FourthDecodeDetail.setDescription(jsonArray.getJSONObject(i).getJSONObject("ext").getString("more_desc"));
                    FourthDecodeDetail.setSpidertime(new Date());
                    FourthDecodeDetail.setTaskbuildtime(taskBuildTime);
                    switch (FourthDecodeDetail.getScene_layer()) {
                        case 4:
                            FourthResultDetail.getLayerFourList().add(FourthDecodeDetail);
                            break;
                        case 5:
                            FourthResultDetail.getLayerFiveList().add(FourthDecodeDetail);
                            break;
                        case 6:
                            FourthResultDetail.getLayerSixList().add(FourthDecodeDetail);
                            break;
                    }
                }
            }
            //解析剩余页Json
            if (page_count > 1) {
                String currentUrl;
                for (int i = 2; i <= page_count; i++) {
                    currentUrl = baseUrl.replaceAll("#", taskDetail.getName());
                    currentUrl = currentUrl.replaceAll("@", Integer.toString(i));
                    jsonresult = getResponse(currentUrl);
                    if (jsonresult.getInt("errno") != 0) {
                        log.info("第四步第" + i + "页链接" + currentUrl + "没有Json数据！");
                    } else {
                        jdata = jsonresult.getJSONObject("data");
                        scene_total = jdata.getInt("scene_total");
                        JSONArray jsonArray = jdata.getJSONArray("scene_list");
                        try {
                            for (int j = 0; j < jsonArray.length(); j++) {

                                SightInfoBaiduLayerDecodeDetail FourthDecodeDetail = new SightInfoBaiduLayerDecodeDetail();
                                FourthDecodeDetail.setName(jsonArray.getJSONObject(j).getString("sname"));
                                FourthDecodeDetail.setUrl("lvyou.baidu.com/" + jsonArray.getJSONObject(j).getString("surl"));
                                FourthDecodeDetail.setEn_name(jsonArray.getJSONObject(j).getString("surl"));
                                FourthDecodeDetail.setScene_layer(jsonArray.getJSONObject(j).getInt("scene_layer"));
                                FourthDecodeDetail.setMap_x(jsonArray.getJSONObject(j).getJSONObject("ext").getString("map_x"));
                                FourthDecodeDetail.setMap_y(jsonArray.getJSONObject(j).getJSONObject("ext").getString("map_y"));
                                FourthDecodeDetail.setDescription(jsonArray.getJSONObject(j).getJSONObject("ext").getString("more_desc"));
                                FourthDecodeDetail.setSpidertime(new Date());
                                FourthDecodeDetail.setTaskbuildtime(taskBuildTime);
                                switch (FourthDecodeDetail.getScene_layer()) {
                                    case 4:
                                        FourthResultDetail.getLayerFourList().add(FourthDecodeDetail);
                                        break;
                                    case 5:
                                        FourthResultDetail.getLayerFiveList().add(FourthDecodeDetail);
                                        break;
                                    case 6:
                                        FourthResultDetail.getLayerSixList().add(FourthDecodeDetail);
                                        break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            log.info("第四步解析" + currentUrl + "页出错！");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("============Exception Info:" + e.getMessage());
            log.info("第四步解析Json出错！");
        }
        FourthTaskResult.setTask(task);
        FourthTaskResult.setResultDetail(FourthResultDetail);
        return FourthTaskResult;
    }

    private JSONObject getResponse(String url) {
        JSONObject json = null;
        Executor executor = new Executor();
        Command command = new Command();
        command.setUrl(url);
        command.addHeader("Accept", "text/html");
        command.addHeader("Accept-Encoding", "gzip");
        command.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        command.addHeader("Connection", "keep-alive");
        command.addHeader("DNT", "1");
        command.addHeader("Host", "lvyou.baidu.com");
        command.addHeader("Referer", "http://lvyou.baidu.com");
        command.addHeader("User-Agent", NetworkUtils.getNewerUserAgentrandomly());
        Response response = new Response();
        executor.executeGetGzip(command, response);
        String content = null;
        if (response.getResultCode() == Response.Success && response.getContent() != null) {
            content = new String(response.getContent());
        } else {
            log.info("第四步获取" + url + "Json数据出错！");
            return null;
        }
        if (content != null) {
            try {
                json = new JSONObject(content.substring(content.indexOf("{"), content.lastIndexOf("}") + 1));
                return json;
            } catch (Exception e) {
                e.printStackTrace();
                log.info("第四步获取" + url + "Json数据出错！");
            }
        }
        return null;
    }
}