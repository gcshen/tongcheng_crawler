package com.ctrip.baidulvyou.steptwo;

import com.ctrip.fx.enteroctopus.common.module.TaskDetail;

/**
 * Created by gcshen on 14-1-7.
 */
public class SightBaiduSecondTaskDetail extends TaskDetail {
    private String name;
    private long taskbuildtime;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getTaskbuildtime() {
        return taskbuildtime;
    }

    public void setTaskbuildtime(long taskbuildtime) {
        this.taskbuildtime = taskbuildtime;
    }

    public String getName() {
        return name;
    }

    public void setName(String url) {
        this.name = url;
    }
}
