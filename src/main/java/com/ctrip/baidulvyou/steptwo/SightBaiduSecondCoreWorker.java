package com.ctrip.baidulvyou.steptwo;

import com.ctrip.baidulvyou.stepone.SightInfoBaiduLayerDecodeDetail;
import com.ctrip.common.ResultFactory;
import com.ctrip.network.Command;
import com.ctrip.network.Executor;
import com.ctrip.network.Response;
import com.ctrip.crawler.ICoreWorker;
import com.ctrip.fx.enteroctopus.common.module.Result;
import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.fx.enteroctopus.common.util.NetworkUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by gcshen on 14-1-7.
 */
public class SightBaiduSecondCoreWorker implements ICoreWorker {
    private static Logger log = LoggerFactory.getLogger(SightBaiduSecondCoreWorker.class);

    @Override
    public Result execute(Task task) {
        ResultFactory rf = ResultFactory.instance();
        Result SecondTaskResult = rf.getResult(task);
        SightBaiduSecondResultDetail SecondResultDetail = new SightBaiduSecondResultDetail();
        String baseUrl = "http://lvyou.baidu.com/destination/ajax/allview?surl=#&format=ajax&cid=0&pn=@";
        try {
            SightBaiduSecondTaskDetail taskDetail = (SightBaiduSecondTaskDetail) task.getTaskDetail();
            long taskBuildTime = task.getBuildTime();
            String url = taskDetail.getUrl();
            //抓取Json数据
            JSONObject jsonresult = getResponse(url);
            JSONObject jdata;
            int scene_total;
            int page_count;
            //解析第一页Json
            if (jsonresult.getInt("errno") != 0) {
                log.info("第二步第一页链接" + taskDetail.getUrl() + "没有Json数据！");
                return null;
            } else {
                jdata = jsonresult.getJSONObject("data");
                scene_total = jdata.getInt("scene_total");
                if(scene_total==0)
                {
                    SightInfoBaiduLayerDecodeDetail SecondDecodeDetail = new SightInfoBaiduLayerDecodeDetail();
                    SecondDecodeDetail.setName(taskDetail.getName());
                    SecondDecodeDetail.setSpidertime(new Date());
                    SecondDecodeDetail.setTaskbuildtime(taskDetail.getTaskbuildtime());
                    SecondResultDetail.getLayerSixList().add(SecondDecodeDetail);
                    SecondTaskResult.setTask(task);
                    SecondTaskResult.setResultDetail(SecondResultDetail);
                    return SecondTaskResult;
                }
                JSONArray jsonArray = jdata.getJSONArray("scene_list");
                if (scene_total / jsonArray.length() >= 1 && scene_total != jsonArray.length())
                    page_count = scene_total / jsonArray.length() + 1;
                else
                    page_count = 1;
                for (int i = 0; i < jsonArray.length(); i++) {
                    SightInfoBaiduLayerDecodeDetail SecondDecodeDetail = new SightInfoBaiduLayerDecodeDetail();
                    SecondDecodeDetail.setName(jsonArray.getJSONObject(i).getString("sname"));
                    SecondDecodeDetail.setUrl("lvyou.baidu.com/" + jsonArray.getJSONObject(i).getString("surl"));
                    SecondDecodeDetail.setEn_name(jsonArray.getJSONObject(i).getString("surl"));
                    SecondDecodeDetail.setScene_layer(jsonArray.getJSONObject(i).getInt("scene_layer"));
                    SecondDecodeDetail.setMap_x(jsonArray.getJSONObject(i).getJSONObject("ext").getString("map_x"));
                    SecondDecodeDetail.setMap_y(jsonArray.getJSONObject(i).getJSONObject("ext").getString("map_y"));
                    SecondDecodeDetail.setDescription(jsonArray.getJSONObject(i).getJSONObject("ext").getString("more_desc"));
                    SecondDecodeDetail.setSpidertime(new Date());
                    SecondDecodeDetail.setTaskbuildtime(taskBuildTime);
                    switch (SecondDecodeDetail.getScene_layer()) {
                        case 4:
                            SecondResultDetail.getLayerFourList().add(SecondDecodeDetail);
                            break;
                        case 5:
                            SecondResultDetail.getLayerFiveList().add(SecondDecodeDetail);
                            break;
                        case 6:
                            SecondResultDetail.getLayerSixList().add(SecondDecodeDetail);
                            break;
                    }
                }
            }
            //解析剩余页Json
            if (page_count > 1) {
                String currentUrl;
                for (int i = 2; i <= page_count; i++) {
                    currentUrl = baseUrl.replaceAll("#", taskDetail.getName());
                    currentUrl = currentUrl.replaceAll("@", Integer.toString(i));
                    jsonresult = getResponse(currentUrl);
                    if (jsonresult.getInt("errno") != 0) {
                        log.info("第二步第" + i + "页链接" + currentUrl + "没有Json数据！");
                    } else {
                        jdata = jsonresult.getJSONObject("data");
                        scene_total = jdata.getInt("scene_total");
                        JSONArray jsonArray = jdata.getJSONArray("scene_list");
                        try {
                            for (int j = 0; j < jsonArray.length(); j++) {

                                SightInfoBaiduLayerDecodeDetail SecondDecodeDetail = new SightInfoBaiduLayerDecodeDetail();
                                SecondDecodeDetail.setName(jsonArray.getJSONObject(j).getString("sname"));
                                SecondDecodeDetail.setUrl("lvyou.baidu.com/" + jsonArray.getJSONObject(j).getString("surl"));
                                SecondDecodeDetail.setEn_name(jsonArray.getJSONObject(j).getString("surl"));
                                SecondDecodeDetail.setScene_layer(jsonArray.getJSONObject(j).getInt("scene_layer"));
                                SecondDecodeDetail.setMap_x(jsonArray.getJSONObject(j).getJSONObject("ext").getString("map_x"));
                                SecondDecodeDetail.setMap_y(jsonArray.getJSONObject(j).getJSONObject("ext").getString("map_y"));
                                SecondDecodeDetail.setDescription(jsonArray.getJSONObject(j).getJSONObject("ext").getString("more_desc"));
                                SecondDecodeDetail.setSpidertime(new Date());
                                SecondDecodeDetail.setTaskbuildtime(taskBuildTime);
                                switch (SecondDecodeDetail.getScene_layer()) {
                                    case 4:
                                        SecondResultDetail.getLayerFourList().add(SecondDecodeDetail);
                                        break;
                                    case 5:
                                        SecondResultDetail.getLayerFiveList().add(SecondDecodeDetail);
                                        break;
                                    case 6:
                                        SecondResultDetail.getLayerSixList().add(SecondDecodeDetail);
                                        break;
                                }
                            }
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                            log.info("第二步解析"+currentUrl+"页出错！");
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("============Exception Info:" + e.getMessage());
            log.info("第二步解析Json出错！");
        }
        SecondTaskResult.setTask(task);
        SecondTaskResult.setResultDetail(SecondResultDetail);
        return SecondTaskResult;
    }

    private JSONObject getResponse(String url) {
        JSONObject json = null;
        Executor executor = new Executor();
        Command command = new Command();
        command.setUrl(url);
        command.addHeader("Accept", "text/html");
        command.addHeader("Accept-Encoding", "gzip");
        command.addHeader("Accept-Language", "zh-CN,zh;q=0.8,en-us;q=0.5,en;q=0.3");
        command.addHeader("Connection", "keep-alive");
        command.addHeader("DNT", "1");
        command.addHeader("Host", "lvyou.baidu.com");
        command.addHeader("Referer", "http://lvyou.baidu.com");
        command.addHeader("User-Agent", NetworkUtils.getNewerUserAgentrandomly());
        Response response = new Response();
        executor.executeGetGzip(command, response);
        String content = null;
        if (response.getResultCode() == Response.Success && response.getContent() != null) {
            content = new String(response.getContent());
        } else {
            log.info("第二步获取"+url+"Json数据出错！");
            return null;
        }
        if (content != null) {
            try {
                json = new JSONObject(content.substring(content.indexOf("{"), content.lastIndexOf("}") + 1));
                return json;
            } catch (Exception e) {
                e.printStackTrace();
                log.info("第二步获取"+url+"Json数据出错！");
            }
        }
        return null;
    }
}
