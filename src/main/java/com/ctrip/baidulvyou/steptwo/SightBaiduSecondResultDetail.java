package com.ctrip.baidulvyou.steptwo;

import com.ctrip.baidulvyou.stepone.SightInfoBaiduLayerDecodeDetail;
import com.ctrip.fx.enteroctopus.common.module.ResultDetail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gcshen on 14-1-7.
 */
public class SightBaiduSecondResultDetail extends ResultDetail<SightInfoBaiduLayerDecodeDetail> {
    private List<SightInfoBaiduLayerDecodeDetail> layerFourList=new ArrayList<SightInfoBaiduLayerDecodeDetail>();
    private List<SightInfoBaiduLayerDecodeDetail> layerFiveList=new ArrayList<SightInfoBaiduLayerDecodeDetail>();
    private List<SightInfoBaiduLayerDecodeDetail> layerSixList=new ArrayList<SightInfoBaiduLayerDecodeDetail>();

    public List<SightInfoBaiduLayerDecodeDetail> getLayerFourList() {
        return layerFourList;
    }

    public void setLayerFourList(List<SightInfoBaiduLayerDecodeDetail> layerFourList) {
        this.layerFourList = layerFourList;
    }

    public List<SightInfoBaiduLayerDecodeDetail> getLayerFiveList() {
        return layerFiveList;
    }

    public void setLayerFiveList(List<SightInfoBaiduLayerDecodeDetail> layerFiveList) {
        this.layerFiveList = layerFiveList;
    }

    public List<SightInfoBaiduLayerDecodeDetail> getLayerSixList() {
        return layerSixList;
    }

    public void setLayerSixList(List<SightInfoBaiduLayerDecodeDetail> layerSixList) {
        this.layerSixList = layerSixList;
    }
}
