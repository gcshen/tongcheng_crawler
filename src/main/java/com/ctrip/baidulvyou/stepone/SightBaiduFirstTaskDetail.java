package com.ctrip.baidulvyou.stepone;


import com.ctrip.fx.enteroctopus.common.module.TaskDetail;

/**
 * Created by gcshen on 14-1-6.
 */
public class SightBaiduFirstTaskDetail extends TaskDetail {
     private  String url;
    private String name;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
