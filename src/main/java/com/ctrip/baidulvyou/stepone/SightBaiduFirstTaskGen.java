package com.ctrip.baidulvyou.stepone;

import com.ctrip.fx.enteroctopus.common.module.Task;
import com.ctrip.localTaskGenerator.IGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gcshen on 14-1-6.
 */
public class SightBaiduFirstTaskGen implements IGenerator {
    private static List<SightBaiduFirstTaskDetail> taskList = getTasks();
    private static int taskIndex = 0;
    private static Logger log = LoggerFactory.getLogger(SightBaiduFirstTaskGen.class);
    private static long taskbuidtime=System.currentTimeMillis();

    @Override
    public Task genTask() {
        if (taskIndex == taskList.size()) {
           log.info("第一步任务完成！");
            return null;
        }
        SightBaiduFirstTaskDetail taskDetail = taskList.get(taskIndex);
        taskIndex++;

        Task task = new Task();
        task.setId(taskIndex);
        task.setBuilder("SightBaiduFirstTest Builder");
        task.setBuildTime(taskbuidtime);
        task.setLastModify(System.currentTimeMillis());
        task.setTaskDetail(taskDetail);
        return task;
    }

    @Override
    public String genTaskStr() {
        return null;
    }
    private static List<SightBaiduFirstTaskDetail> getTasks() {
        List<SightBaiduFirstTaskDetail> tasks = new ArrayList<SightBaiduFirstTaskDetail>();
        //国外景点7大洲，只需7条url
        //String [] continents={"yazhou","ouzhou","beimeizhou","nanmeizhou","feizhou","dayangzhou","nanjizhou"};
        //String url = "http://lvyou.baidu.com/destination/ajax/allview?surl=#&format=ajax&cid=0&pn=1";
        //String continentUrl;
        //七大洲
        /*for(int i=0;i<continents.length;i++)
        {
            SightBaiduFirstTaskDetail taskDetail = new SightBaiduFirstTaskDetail();
            continentUrl=url.replaceAll("#",continents[i]);
            taskDetail.setUrl(continentUrl);
            taskDetail.setName(continents[i]);
            tasks.add(taskDetail);
        }*/
        //获取国内景点，从zhongguo开始，第一步只需一条数据
        String url = "http://lvyou.baidu.com/destination/ajax/allview?surl=zhongguo&format=ajax&cid=0&pn=1";
        SightBaiduFirstTaskDetail taskDetail = new SightBaiduFirstTaskDetail();
        taskDetail.setUrl(url);
        taskDetail.setName("zhongguo");
        tasks.add(taskDetail);

        return tasks;
    }
}
