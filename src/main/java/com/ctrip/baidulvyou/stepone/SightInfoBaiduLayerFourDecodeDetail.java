package com.ctrip.baidulvyou.stepone;

import java.util.Date;

/**
 * Created by gcshen on 14-1-6.
 */
public class SightInfoBaiduLayerFourDecodeDetail extends SightInfoBaiduLayerDecodeDetail{
    private long id;
    private String name;
    private String url;
    private int scene_layer;
    private String en_name;
    private String description;
    private String map_x;
    private String map_y;
    private Date spidertime;
    private long taskbuildtime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getScene_layer() {
        return scene_layer;
    }

    public void setScene_layer(int scene_layer) {
        this.scene_layer = scene_layer;
    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMap_x() {
        return map_x;
    }

    public void setMap_x(String map_x) {
        this.map_x = map_x;
    }

    public String getMap_y() {
        return map_y;
    }

    public void setMap_y(String map_y) {
        this.map_y = map_y;
    }

    public Date getSpidertime() {
        return spidertime;
    }

    public void setSpidertime(Date spidertime) {
        this.spidertime = spidertime;
    }

    public long getTaskbuildtime() {
        return taskbuildtime;
    }

    public void setTaskbuildtime(long taskbuildtime) {
        this.taskbuildtime = taskbuildtime;
    }
}
